#include "../Common/CommonFunctions.h"

// Client's IP(v4) address
static const char CLIENT_IP[] = "10.131.51.24";
// Server's port
static const short SERVER_PORT = 3000;
// Client's port
static const short CLIENT_PORT = 3001;

int main()
{
	// Initialize library
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		return Cleanup();
#pragma region local endpoint
	//set up the server endpoint
	hostent * localhost = gethostbyname("");
	char * localIP = inet_ntoa(*(reinterpret_cast<in_addr*>(*localhost->h_addr_list)));

	// Create local endpoint info
	sockaddr_in serverAddress;
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(SERVER_PORT);
	serverAddress.sin_addr.S_un.S_addr = inet_addr(localIP);
	//debug server info print out
	// Display some info
	std::cout << "Hi, I'm server" << std::endl;
	std::cout << "Localhost: " << localhost->h_name << std::endl;
	std::cout << "Local IP: " << localIP;
	std::cout << " - Port: " << serverAddress.sin_port << std::endl;
	// display the local directory contents
	std::system("dir .");
#pragma endregion

#pragma region Socket_Binding
	// Create UDP server socket
	SOCKET serverSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (serverSocket == INVALID_SOCKET)
		return Cleanup();
	// Bind the socket to local endpoint
	// We have to bind this socket in order for it to be open to receiving incoming messages
	if (bind(serverSocket, reinterpret_cast<sockaddr*>(&serverAddress), sizeof(serverAddress)) == SOCKET_ERROR)
		return Cleanup();
#pragma endregion

	//variables for transmitting data
	char buffer[BUFFER_SIZE] = { 0 };
	int recvBytes = 0;
	int sentBytes = 0;
	//store remote endpoint
	sockaddr_in remoteAddress;
	int remoteAddressSize = sizeof(remoteAddress);
#pragma region GettingFileName
	// Get filename request
	std::cout << "Waiting for file to send" << std::endl;
	if (::recvfrom(serverSocket, buffer, BUFFER_SIZE, 0,
		reinterpret_cast<sockaddr*>(&remoteAddress),
		&remoteAddressSize) == SOCKET_ERROR)
		return Cleanup();
	try
	{
		// get size of file and print it (for verification)
		int size = FileSize(buffer);
		std::cout << "File " << buffer << " requested: " << size << " bytes" << std::endl;
		//now we send the file
		// Send file to the requester's endpoint
		int result = SendFile(buffer, serverSocket, remoteAddress, size);
		std::cout << result << " bytes sent" << std::endl;

	}
	catch (const char * e) { // Handle user exceptions
		std::cout << "Exception: " << e << std::endl;
		return Cleanup();
	}
#pragma endregion

	// Cleanup & close everything
	shutdown(serverSocket, SD_BOTH);
	closesocket(serverSocket);
	return 0;
}