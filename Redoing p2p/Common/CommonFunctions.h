#define WIN32_LEAN_AND_MEAN

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

#include <iostream>
#include <fstream> // ofstream, write file

// link the winsock library
#pragma comment (lib, "ws2_32.lib")
// Size of network buffer

static const int BUFFER_SIZE = 512;
int Cleanup()
{
	int err = WSAGetLastError();
	if (err)
	{
		std::cout << "Winsock error: " << err << std::endl;
	}
	system("pause");
	return err;
}
// Returns the size (in bytes) of a specified local file
int FileSize(const char *filename)
{
	std::ifstream file(filename);
	if (!file.good())
		throw "Error checking file size";

	std::streamoff begin = file.tellg();
	file.seekg(0, std::ios::end);
	std::streamoff end = file.tellg();
	file.close();

	return static_cast<int>(end - begin);
}
// Sends a (binary) file named 'filename' with size = 'size' bytes
// to a particular destination 'dest' using UDP socket 's'
int SendFile(const char *filename, SOCKET s, sockaddr_in dest, int size)
{
	// Variables to handle the transmission
	char buffer[BUFFER_SIZE] = { 0 };
	// open the desired file and check its validity
	std::ifstream file(filename, std::ios::binary);
	if (!file.is_open())
		throw "Error opening file";
	// some required local variables
	char		sendBuffer[BUFFER_SIZE] = { 0 };	// buffer to send
	unsigned int	bytesSent = 0;	// counter for total sent bytes
	int			result = 0;	// result of the sendto
	int			chunk = 0;	// amount of data to read and send
	//server has to send to the client the total size of the file
	int fileSize = FileSize(filename);
	std::cout << "File size of " << filename << "is: " << fileSize << std::endl;
	// Send data payload
	std::cout << "Server sending " << fileSize << " bytes of data to the client" << std::endl;
	buffer[0] = fileSize;
	int bytes_sent = ::sendto(s, sendBuffer, fileSize, 0, reinterpret_cast<sockaddr*>(&dest), sizeof(dest));
	//error checking
	if (bytes_sent == SOCKET_ERROR) // SOCKET_ERROR = -1
		throw "Error sending file";
	return bytesSent;
}